package com.commit451.gitlab.fragment

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.commit451.gitlab.App
import com.commit451.gitlab.activity.PickBranchOrTagActivity
import com.commit451.gitlab.adapter.TagAdapter
import com.commit451.gitlab.databinding.FragmentPickTagBinding
import com.commit451.gitlab.extension.with
import com.commit451.gitlab.model.Ref
import com.commit451.gitlab.model.api.Tag
import timber.log.Timber

/**
 * Pick a branch, any branch
 */
class PickTagFragment : BaseFragment() {

    companion object {

        private const val EXTRA_PROJECT_ID = "project_id"
        private const val EXTRA_CURRENT_REF = "current_ref"

        fun newInstance(projectId: Long, ref: Ref?): PickTagFragment {
            val fragment = PickTagFragment()
            val args = Bundle()
            args.putLong(EXTRA_PROJECT_ID, projectId)
            args.putParcelable(EXTRA_CURRENT_REF, ref)
            fragment.arguments = args
            return fragment
        }
    }

    private var binding: FragmentPickTagBinding? = null
    private lateinit var adapterTags: TagAdapter

    private var projectId: Long = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        projectId = arguments?.getLong(EXTRA_PROJECT_ID)!!
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentPickTagBinding.inflate(inflater, container, false)
        return binding!!.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val ref = arguments?.getParcelable<Ref>(EXTRA_CURRENT_REF)!!
        adapterTags = TagAdapter(ref, object : TagAdapter.Listener {
            override fun onTagClicked(entry: Tag) {
                val data = Intent()
                val newRef = Ref(Ref.TYPE_TAG, entry.name)
                data.putExtra(PickBranchOrTagActivity.EXTRA_REF, newRef)
                activity?.setResult(Activity.RESULT_OK, data)
                activity?.finish()
            }
        })
        binding?.listProjects?.layoutManager = LinearLayoutManager(activity)
        binding?.listProjects?.adapter = adapterTags

        loadData()
    }

    override fun loadData() {
        if (view == null) {
            return
        }
        binding?.progress?.visibility = View.VISIBLE
        binding?.textMessage?.visibility = View.GONE

        App.get().gitLab.getTags(projectId)
            .with(this)
            .subscribe({
                binding?.progress?.visibility = View.GONE
                adapterTags.setEntries(it)
            }, {
                Timber.e(it)
                binding?.progress?.visibility = View.GONE
                binding?.textMessage?.visibility = View.VISIBLE
            })
    }
}
